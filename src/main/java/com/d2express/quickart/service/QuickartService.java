package com.d2express.quickart.service;

import com.d2express.quickart.model.CartData;
import com.d2express.quickart.model.Orders;
import com.d2express.quickart.model.Response;
import com.d2express.quickart.model.User;

public interface QuickartService {

	public Response registerUser(User user);
	
	public void createOrder(Orders order);
	
	public Response getItemDetails(long itemId);
	
	public Response checkIfMobileExists(String phoneNumber);
	
	public Response saveCartData(CartData cartData);
	
	public Response getCartData(String cartId);
}

