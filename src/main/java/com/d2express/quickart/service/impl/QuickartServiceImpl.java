package com.d2express.quickart.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.d2express.quickart.common.QuickartConstants;
import com.d2express.quickart.dao.QuickartAppDao;
import com.d2express.quickart.model.CartData;
import com.d2express.quickart.model.InventoryItem;
import com.d2express.quickart.model.Orders;
import com.d2express.quickart.model.Response;
import com.d2express.quickart.model.User;
import com.d2express.quickart.service.QuickartService;


@Service
public class QuickartServiceImpl implements QuickartService {

	@Autowired
	private QuickartAppDao quickartDao;
	
	@Override
	public Response registerUser(User user) {
		
		String registerResponse=quickartDao.registerUser(user);
		Response response = new Response();
		
		switch (registerResponse) {
		case QuickartConstants.MOBILE_NUMBER_ALREADY_EXISTS:
			response.setStatusCode(HttpStatus.BAD_REQUEST.value());
			response.setMsg(QuickartConstants.MOBILE_NUMBER_ALREADY_EXISTS);
			break;
		default:
			response.setStatusCode(HttpStatus.OK.value());
			response.setMsg(QuickartConstants.SUCCESS);
			break;
		}

		return response;
	}

	@Override
	public void createOrder(Orders order) {
		quickartDao.createOrder(order);
	}

	@Override
	public Response getItemDetails(long itemId) {
		
		Optional<InventoryItem> itemDetails = quickartDao.getItemDetails(itemId);
		Response response = new Response();
		if(!itemDetails.isPresent())
		{
			response.setStatusCode(HttpStatus.NOT_FOUND.value());
			response.setMsg(QuickartConstants.ITEM_NOT_FOUND_DESC);
			return  response;
		}
			response.setStatusCode(HttpStatus.OK.value());
			response.setMsg(QuickartConstants.ITEM_FOUND_DESC);
			response.setData(itemDetails.get());
			
		return response;
	}

	@Override
	public Response checkIfMobileExists(String phoneNumber) {
		
		boolean mobileExists = quickartDao.checkIfMobileNumberExists(phoneNumber);
		
		Response response = new Response();
		
		if(mobileExists)
		{
			response.setStatusCode(HttpStatus.OK.value());
			response.setMsg(QuickartConstants.MOBILE_NUMBER_ALREADY_EXISTS);
		}
		else {
			response.setStatusCode(HttpStatus.NOT_FOUND.value());
			response.setMsg(QuickartConstants.MOBILE_NUMBER_NOT_FOUND);
		}
		
		return response;
	}

	@Override
	public Response saveCartData(CartData cartData) {
		
		String saveCartResponse=quickartDao.saveCartData(cartData);
		Response response = new Response();
		response.setStatusCode(HttpStatus.OK.value());
		response.setMsg(saveCartResponse);
		
		return response;
	}
	
	@Override
	public Response getCartData(String cartId) {
		
		Optional<CartData> cartData = quickartDao.getCartData(cartId);
		Response response = new Response();
		if(!cartData.isPresent())
		{
			response.setStatusCode(HttpStatus.NOT_FOUND.value());
			response.setMsg(QuickartConstants.CART_NOT_VALID_DESC);
			return  response;
		}
			response.setStatusCode(HttpStatus.OK.value());
			response.setMsg(QuickartConstants.CART_FOUND_DESC);
			response.setData(cartData.get());
		
		return response;
	}

}
