package com.d2express.quickart.common;

public interface QuickartConstants {

	public String ITEM_NOT_FOUND_DESC = "The requested item could not be found";
	public String CART_NOT_VALID_DESC = "The requested cart could not be found";
	public String ITEM_FOUND_DESC="Found Requested Item";
	public String CART_FOUND_DESC="Found Requested Cart";
	public String MOBILE_NUMBER_ALREADY_EXISTS="Mobile number already registered";
	public String MOBILE_NUMBER_NOT_FOUND="Mobile number not found";
	public String MOBILE_NUMBER_EMTPY="Invalid Request.Mobile Number is empty";
	public String SUCCESS = "SUCCESS";
}
