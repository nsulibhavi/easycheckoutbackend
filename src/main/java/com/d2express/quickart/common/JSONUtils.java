package com.d2express.quickart.common;

import org.json.JSONObject;

public class JSONUtils {

	public static String getValue(String data,String parameter)
	{
		JSONObject jsonObj = new JSONObject(data);
		JSONObject dataObj = jsonObj.getJSONObject("data");
		String value = dataObj.getString(parameter);
		return value;
		
	}
	
	public static JSONObject getJsonObject(String data)
	{
		JSONObject jsonObj = new JSONObject(data);
		JSONObject dataObj = jsonObj.getJSONObject("data");
		return dataObj;
	}
	
}


