package com.d2express.quickart.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table
public class CartData {

	@Id
	@Column(unique = true)
	private String cartId;
	
	@OneToMany(cascade = {CascadeType.ALL})
	@JoinColumn(name="cartId")
	@Column
	private Set<CartItem> cartItems;

	
	public String getCartId() {
		return cartId;
	}

	
	public void setCartId(String cartId) {
		this.cartId = cartId;
	}

	public Set<CartItem> getCartItems() {
		return cartItems;
	}

	public void setCartItems(Set<CartItem> cartItems) {
		this.cartItems = cartItems;
	}
	
}
