/*
 * Created on 07/02/2021 
 * User class
 */

package com.d2express.quickart.model;

import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table
public class Orders {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long orderId;
	
	@Column()
	@CreationTimestamp
	private LocalDateTime orderDate;
	
	@Column
	private long orderAmount;
	
	@OneToMany
	@JoinColumn(name="orderDetailId")
	private Set<OrderDetails> orderDetails;
	
	@ManyToOne
    @JoinColumn(name="userId")
    private User user;
	

	public long getOrderId() {
		return orderId;
	}

	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}

	public LocalDateTime getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(LocalDateTime orderDate) {
		this.orderDate = orderDate;
	}

	public long getOrderAmount() {
		return orderAmount;
	}

	public void setOrderAmount(long orderAmount) {
		this.orderAmount = orderAmount;
	}

	public Set<OrderDetails> getOrderDetails() {
		return orderDetails;
	}

	public void setOrderDetails(Set<OrderDetails> orderDetails) {
		this.orderDetails = orderDetails;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
			
}
