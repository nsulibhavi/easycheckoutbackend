package com.d2express.quickart.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table
public class PurchaseItem {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long purchaseItemId;
	
	@OneToOne
	@JoinColumn(name="itemId")
	private InventoryItem inventory;
	
	@Column
	private String purchaseQuantity;

	public InventoryItem getInventory() {
		return inventory;
	}

	public void setInventory(InventoryItem inventory) {
		this.inventory = inventory;
	}

	public String getPurchaseQuantity() {
		return purchaseQuantity;
	}

	public void setPurchaseQuantity(String purchaseQuantity) {
		this.purchaseQuantity = purchaseQuantity;
	}

	public long getPurchaseItemId() {
		return purchaseItemId;
	}

	public void setPurchaseItemId(long purchaseItemId) {
		this.purchaseItemId = purchaseItemId;
	}
	
	
}
