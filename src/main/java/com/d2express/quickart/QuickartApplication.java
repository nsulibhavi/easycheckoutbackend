package com.d2express.quickart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuickartApplication {

	public static void main(String[] args) {
		SpringApplication.run(QuickartApplication.class, args);
	}

}
