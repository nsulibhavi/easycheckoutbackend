package com.d2express.quickart.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.d2express.quickart.model.User;

@Repository
public interface QuickartUserRepository extends CrudRepository<User, Long> {

	Optional<User> findByMobileNumber(String mobileNumber);

}
