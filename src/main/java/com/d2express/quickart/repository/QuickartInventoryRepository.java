package com.d2express.quickart.repository;

import org.springframework.data.repository.CrudRepository;

import com.d2express.quickart.model.InventoryItem;

public interface QuickartInventoryRepository  extends CrudRepository<InventoryItem, Long>  {

}
