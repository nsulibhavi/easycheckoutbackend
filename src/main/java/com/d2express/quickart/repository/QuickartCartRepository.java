package com.d2express.quickart.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.d2express.quickart.model.CartData;
import com.d2express.quickart.model.User;

@Repository
public interface QuickartCartRepository extends CrudRepository<CartData, Long> {

	Optional<CartData> findByCartId(String cartId);

}
