package com.d2express.quickart.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.d2express.quickart.model.Orders;
import com.d2express.quickart.model.User;

@Repository
public interface QuickartOrderRepository extends CrudRepository<Orders, Long> {

	

}
