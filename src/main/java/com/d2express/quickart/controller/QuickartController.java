/*
 * Controller class for Quickart API application
 * 
 */

package com.d2express.quickart.controller;


import java.util.Map;
import java.util.Random;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JsonParserFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.d2express.quickart.common.JSONUtils;
import com.d2express.quickart.common.QuickartConstants;
import com.d2express.quickart.model.CartData;
import com.d2express.quickart.model.Response;
import com.d2express.quickart.model.User;
import com.d2express.quickart.service.QuickartService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
@RequestMapping("/d2express")
@RestController
public class QuickartController {
	
	Logger logger = LoggerFactory.getLogger(QuickartController.class);
	
	@Autowired
	private QuickartService quickartService;
	
	@RequestMapping("/register")
	@PostMapping(consumes = "application/json"
	,produces = "application/json")	
	public ResponseEntity<Response> registerUser(@RequestBody User user) {

		logger.info("Invoking register user API");		
		Response response = quickartService.registerUser(user);
		return new ResponseEntity<Response>(response, HttpStatus.valueOf(response.getStatusCode()));
		
	}
	
		
	@RequestMapping("/getInventoryItem")
	@GetMapping(produces="application/json")
	public ResponseEntity<Response> getInventoryItem(@RequestParam String id) {

		logger.info("Invoking getInventoryItem API");
		/*For now generate a randomn number between 1 to 100 as a temporary workaround for demo*/
		Random rand = new Random(); 
		int upperBound = 100;
		int lowerBound = 1;
		int rItemId = rand.nextInt(upperBound - lowerBound) + lowerBound;
		/*Temporary changes end*/
		//Response response  = quickartService.getItemDetails(Long.parseLong(id));/* Uncomment while implementing*/
		
		logger.info("Fetching details for "+rItemId);
		Response response  = quickartService.getItemDetails(rItemId);

		return new ResponseEntity<Response>(response, HttpStatus.valueOf(response.getStatusCode()));
	}
	
	@RequestMapping("/checkMobileExists")
	@PostMapping(produces="application/json")
	public ResponseEntity<Response> checkIfMobileExists(@RequestBody String request) {

		logger.info("Invoking checkIfMobileExists API");

		String phoneNumber = JSONUtils.getValue(request, "phoneNumber");
				
		logger.info("mobile Number" +phoneNumber);
		Response response  = null;
		if(phoneNumber == null || phoneNumber.equalsIgnoreCase(""))
		{
			response = new Response();
			response.setStatusCode(HttpStatus.BAD_REQUEST.value());
			response.setMsg(QuickartConstants.MOBILE_NUMBER_EMTPY);
			return new ResponseEntity<Response>(response, HttpStatus.valueOf(response.getStatusCode()));
		}
		response  = quickartService.checkIfMobileExists(phoneNumber);
		return new ResponseEntity<Response>(response, HttpStatus.valueOf(response.getStatusCode()));
	}
	
	@RequestMapping("/saveCartData")
	@PostMapping(produces="application/json")
	public ResponseEntity<Response> saveCartData(@RequestBody String request) {

		logger.info("Invoking saveCartData API");
		JSONObject jsonObj = JSONUtils.getJsonObject(request);
		
		Response response  = null;
		logger.info(request);
		ObjectMapper mapper = new ObjectMapper();
		CartData cartData=null;
		try {
			cartData = mapper.readValue(jsonObj.toString(), CartData.class);
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		response  = quickartService.saveCartData(cartData);
		return new ResponseEntity<Response>(response, HttpStatus.valueOf(response.getStatusCode()));
	}
	
	@RequestMapping("/getCartData")
	@PostMapping(produces="application/json")
	public ResponseEntity<Response> getCartData(@RequestBody String request) {

		logger.info("Invoking getCartData API");
		String cartId = JSONUtils.getValue(request, "cartId");
		
		Response response  = null;
		logger.info(request);
		
		response  = quickartService.getCartData(cartId);
		return new ResponseEntity<Response>(response, HttpStatus.valueOf(response.getStatusCode()));
	}
	
}
