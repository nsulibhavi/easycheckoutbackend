package com.d2express.quickart.dao.impl;

import java.util.Optional;

import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.d2express.quickart.common.QuickartConstants;
import com.d2express.quickart.dao.QuickartAppDao;
import com.d2express.quickart.model.CartData;
import com.d2express.quickart.model.InventoryItem;
import com.d2express.quickart.model.Orders;
import com.d2express.quickart.model.User;
import com.d2express.quickart.repository.QuickartCartRepository;
import com.d2express.quickart.repository.QuickartInventoryRepository;
import com.d2express.quickart.repository.QuickartOrderRepository;
import com.d2express.quickart.repository.QuickartUserRepository;

@Component
public class QuickartDaoImpl implements QuickartAppDao {

	@Autowired
	QuickartUserRepository userRepository;
	
	@Autowired
	QuickartOrderRepository orderRepository;
	
	@Autowired
	QuickartInventoryRepository inventoryRepository;
	
	@Autowired
	QuickartCartRepository cartRepository;
	
	
	
	public String registerUser(User user) {
		try {
			//Check if mobile number already exists. If no save user  else return false
			
			if(checkIfMobileNumberExists(user.getMobileNumber()))
			{
				return QuickartConstants.MOBILE_NUMBER_ALREADY_EXISTS;
			}
			else {
				userRepository.save(user);
				return QuickartConstants.SUCCESS;
			}
	
		}
		catch(HibernateException e)
		{
			e.printStackTrace();
		}
		
		return "";
		
	}

	@Override
	public void createOrder(Orders order) {
		try {
			orderRepository.save(order);
		}
		catch(HibernateException e)
		{
			e.printStackTrace();
		}
		
	}

	@Override
	public Optional<InventoryItem> getItemDetails(long itemId) {
		
		return inventoryRepository.findById(itemId);
	}
	
	@Override
	public boolean checkIfMobileNumberExists(String mobileNumber) {
		
		if(!userRepository.findByMobileNumber(mobileNumber).isPresent())
		{
			return false;
		}
		
		return true;
	}

	@Override
	public String saveCartData(CartData cartData) {
		
		cartRepository.save(cartData);
		return QuickartConstants.SUCCESS;
	}

	@Override
	public Optional<CartData> getCartData(String cartId) {
		cartRepository.findByCartId(cartId);
		
		return cartRepository.findByCartId(cartId);
	}

}
