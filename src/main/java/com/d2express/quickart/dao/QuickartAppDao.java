package com.d2express.quickart.dao;

import java.util.Optional;

import com.d2express.quickart.model.CartData;
import com.d2express.quickart.model.InventoryItem;
import com.d2express.quickart.model.Orders;
import com.d2express.quickart.model.User;


public interface QuickartAppDao {

	public String registerUser(User user);
	
	public void createOrder(Orders order);
	
	public boolean checkIfMobileNumberExists(String mobileNumber);
	
	public Optional<InventoryItem> getItemDetails(long itemId);
	
	public String saveCartData(CartData cartData);
	
	public Optional<CartData> getCartData(String cartId);
}
